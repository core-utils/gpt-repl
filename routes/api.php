<?php

use DOC\Modules\ContentGenerator\Services\OpenAIGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

Route::get('/send-request', function (
    OpenAIGenerator $openAIGenerator,
    Request $request
) {
    $request->validate([
        'prompt' => 'required',
    ]);

    try {
        $response = $openAIGenerator->generate($request->get('prompt'));
    } catch (Exception $ex) {
        $response = '<b>Exception!!!</b><br>' . $ex->getMessage();
    }

    return response()->json([
        'response' => $response
    ]);
});
