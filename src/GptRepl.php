<?php

namespace Company\GptRepl;

use Laravel\Nova\Nova;
use Laravel\Nova\Tool;

class GptRepl extends Tool
{
    /**
     * Perform any tasks that need to happen when the tool is booted.
     *
     * @return void
     */
    public function boot()
    {
        Nova::script('gpt-repl', __DIR__.'/../dist/js/tool.js');
        Nova::style('gpt-repl', __DIR__.'/../dist/css/tool.css');
    }

    /**
     * Build the view that renders the navigation links for the tool.
     *
     * @return \Illuminate\View\View
     */
    public function renderNavigation()
    {
        return view('gpt-repl::navigation');
    }
}
